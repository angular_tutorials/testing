import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    const title = page.getTitleValue();
    console.log(title);
    expect(page.getParagraphText()).toEqual('Welcome to ' + title + '!');
  });
});
